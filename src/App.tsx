import React from 'react';
import { RouterProvider } from 'react-router';
import { Box, Typography } from '@mui/material';

import { router } from './router/router';

function App() {
  return (
    <Box>
      <Typography variant='h2' textAlign={'center'} p={'3rem'}>
        Image Finder
      </Typography>
      <RouterProvider router={router} />
    </Box>
  );
}

export default App;
