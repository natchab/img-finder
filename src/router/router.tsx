import React from 'react';
import { createBrowserRouter, Navigate } from 'react-router-dom';

import { ImgCard } from '../views/imgCard/imgCard';
import { FinderForm } from '../views/finderForm/finderForm';
import { ImageSelector } from '../views/imageSelector/imageSelector';

export const router = createBrowserRouter([
  {
    path: '*',
    element: <Navigate to='/' />,
  },
  {
    path: '/',
    element: <FinderForm />,
  },
  {
    path: 'select-image',
    element: <ImageSelector />,
  },
  {
    path: 'image',
    element: <ImgCard />,
  },
]);
