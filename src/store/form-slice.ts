import { createSlice } from '@reduxjs/toolkit';

export type FormState = {
  name: string
  surname: string
  topic: string
}

const formInitialState: FormState = { name: '', surname: '', topic: '' }

const formSlice = createSlice({
  name: 'form',
  initialState: formInitialState,
  reducers: {
    setFormData(state, action) {
      state.name = action.payload.name;
      state.surname = action.payload.surname;
      state.topic = action.payload.topic;
    },
  },
});

export const formActions = formSlice.actions;

export default formSlice.reducer;