import {
  AnyAction,
  combineReducers,
  configureStore,
  PreloadedState,
  ThunkAction
} from '@reduxjs/toolkit'
import imageReducer from './image-slice'
import formReducer from './form-slice'

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  undefined,
  AnyAction
>
const rootReducer = combineReducers({
  image: imageReducer,
  form: formReducer
})
export function setupStore(preloadedState?: PreloadedState<RootState>) {
  return configureStore({
    reducer: rootReducer,
    preloadedState
  })
}
export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']