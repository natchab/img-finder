import { createSlice } from '@reduxjs/toolkit';

interface Urls {
  regularUrl: string
  thumbnailUrl: string
}

export interface ImageState {
  urls: Urls
  error: string
}

const imageInitialState: ImageState = {
  urls: {
    regularUrl: '',
    thumbnailUrl: ''
  },
  error: ''
}

const imageSlice = createSlice({
  name: 'image',
  initialState: imageInitialState,
  reducers: {
    setImageData(state, action) {
      state.urls.regularUrl = action.payload.regularUrl;
      state.urls.thumbnailUrl = action.payload.thumbnailUrl;
    },
    resetImageData(state) {
      return imageInitialState
    },
    setError(state,action) {
      state.error = action.payload.error
    }
  },
});

export const imageActions = imageSlice.actions;

export default imageSlice.reducer;