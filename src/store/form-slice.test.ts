import reducer, { formActions, FormState } from './form-slice';

const initialState: FormState = {
  name: '',
  surname: '',
  topic: '',
}

const previousState: FormState = {
  name: 'previous name',
  surname: 'previous surname',
  topic: 'previous topic',
}

test('should return the initial state', () => {
  expect(reducer(undefined, { type: undefined })).toEqual(initialState);
});

test('should correctly update form state', () => {
  const newFormData = {
    name: 'test name',
    surname: 'test surname',
    topic: 'test topic',
  }
  expect(reducer(previousState, formActions.setFormData(newFormData) )).toEqual(newFormData);
});
