import reducer, { imageActions, ImageState } from "./image-slice";

const initialImageState: ImageState = {
  urls: {
    regularUrl: '',
    thumbnailUrl: ''
  },
  error: ''
}

const previousImageState: ImageState = {
  urls: {
    regularUrl: 'previous regular url',
    thumbnailUrl: 'previous thumbnail url'
  },
  error: 'previous error'
}

test("should return the initial state", () => {
  expect(reducer(undefined, { type: undefined })).toEqual(initialImageState);
});

test("should correctly update image state", () => {
  const newRegularUrl = "test regular Url"
  const newThumbnailUrl = "test thumbnail Url"
  const newImageDataPayload = {
    regularUrl: newRegularUrl,
    thumbnailUrl: newThumbnailUrl,
  }
  const newImageData = {
    urls: {
      regularUrl: newRegularUrl,
      thumbnailUrl: newThumbnailUrl
    },
    error: previousImageState.error
  }
  expect(reducer(previousImageState, imageActions.setImageData(newImageDataPayload) )).toEqual(newImageData);
});

test("should correctly reset image state", () => {
  expect(reducer(previousImageState, imageActions.resetImageData())).toEqual(initialImageState);
});

test("should correctly set error state", () => {
  const testErrorMessage = 'test error'
  const testError = {error: testErrorMessage}

  const expectedState = {...previousImageState, error: testErrorMessage}
  expect(reducer(previousImageState, imageActions.setError(testError))).toEqual(expectedState);
});
