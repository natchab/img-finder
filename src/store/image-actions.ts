import { imageActions } from './image-slice';
import { AppDispatch, AppThunk } from '.';

export const fetchImageData: (topic: string) => AppThunk = (topic) => {
  return async (dispatch: AppDispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `https://api.unsplash.com/photos/random?query=${topic}&client_id=${process.env.REACT_APP_UNSPLASH_API_KEY}`
      );

      if (!response.ok) {
        throw new Error('Could not fetch image data!');
      }

      const data = await response.json();
      return data;
    };

    try {
      const imageData = await fetchData();
      dispatch(
        imageActions.setImageData({
          regularUrl: imageData.urls.regular,
          thumbnailUrl: imageData.urls.thumb,
        })
      );
    } catch (error) {
      dispatch(
        imageActions.setError({error: 'Error fetching image data.'})
      );
    }
  };
};
