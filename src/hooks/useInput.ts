import { useReducer } from 'react';

type InputState = {
  value: string;
  isModified: boolean;
};

type Validator = (value: string) => boolean;

type Action =
  | { type: 'INPUT'; value: string }
  | { type: 'BLUR' }
  | { type: 'RESET' };

const initialInputState = {
  value: '',
  isModified: false,
};

const inputStateReducer = (state: InputState, action: Action) => {
  if (action.type === 'INPUT') {
    return { value: action.value, isModified: state.isModified };
  }
  if (action.type === 'BLUR') {
    return { isModified: true, value: state.value };
  }
  if (action.type === 'RESET') {
    return { isModified: false, value: '' };
  }
  return state;
};

const useInput = (validateValue: Validator) => {
  const [inputState, dispatch] = useReducer(
    inputStateReducer,
    initialInputState
  );

  const isValid = validateValue(inputState.value);
  const hasError = !isValid && inputState.isModified;

  const valueChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch({ type: 'INPUT', value: event.target.value });
  };

  const blurHandler = () => {
    dispatch({ type: 'BLUR' });
  };

  const reset = () => {
    dispatch({ type: 'RESET' });
  };

  return {
    value: inputState.value,
    isValid,
    hasError,
    valueChangeHandler,
    blurHandler,
    reset,
  };
};

export default useInput;
