import React from 'react';
import { Box, Alert, Button } from '@mui/material';

type ErrorProps = {
  error: string;
  onErrorClose: () => void;
}

export const ErrorComponent = ({
  error,
  onErrorClose,
}: ErrorProps): JSX.Element => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > *': {
          m: 1,
          p: 1,
        },
      }}
    >
      <Alert severity='error'>{error}</Alert>
      <Button onClick={onErrorClose} variant='outlined'>
        Ok
      </Button>
    </Box>
  );
};
