import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ErrorComponent } from './errorComponent';

const mockErrorClose = jest.fn();

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => mockErrorClose,
}));

describe('Error component should have correct elements', () => {
  test('renders error text', () => {
    const testError = 'test error';
    render(<ErrorComponent error={testError} onErrorClose={mockErrorClose} />);

    const buttons = screen.getAllByRole('button');
    expect(buttons).toHaveLength(1);
    expect(buttons[0].textContent).toBe('Ok');
    expect(screen.getByText(testError)).toBeInTheDocument();
  });

  test('button click calls the onErrorClose function', async () => {
    const testError = 'test error';
    const user = userEvent.setup();
    render(<ErrorComponent error={testError} onErrorClose={mockErrorClose} />);

    const buttons = screen.getAllByRole('button');
    await user.click(buttons[0]);
    expect(mockErrorClose).toHaveBeenCalledTimes(1);
  });
});
