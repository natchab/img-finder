import React, { useEffect, useState } from 'react';
import { Stack, ButtonGroup, Button, CircularProgress } from '@mui/material';
import { useNavigate } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../../hooks/useRedux';
import { fetchImageData } from '../../store/image-actions';
import { ErrorComponent } from '../errorComponent/errorComponent';

export const ImageSelector = () => {
  const [errorLoadingImage, setErrorLoadingImage] = useState('');
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  const dispatch = useAppDispatch();
  const topic = useAppSelector((store) => store.form.topic);
  const imageUrl = useAppSelector((store) => store.image.urls.regularUrl);
  const error = useAppSelector((store) => store.image.error);

  useEffect(() => {
    if (!topic) {
      navigate('/');
    }
  }, [navigate, topic]);

  useEffect(() => {
    dispatch(fetchImageData(topic));
  }, [dispatch, topic]);

  const handleFindAnotherImage = () => {
    setLoading(true);
    dispatch(fetchImageData(topic));
  };

  const handleAccept = () => {
    navigate('/image');
  };

  const handleErrorClose = () => {
    navigate('/');
  };

  const handleImageLoadingError = () => {
    setErrorLoadingImage('Error: Could not load the image.');
  };

  const handleImageLoad = (
    e: React.SyntheticEvent<HTMLImageElement, Event>
  ) => {
    setLoading(false);
  };

  if (error || errorLoadingImage) {
    return <ErrorComponent error={error} onErrorClose={handleErrorClose} />;
  }

  return (
    <Stack
      sx={{
        alignItems: 'center',
        '& > *': {
          m: '1rem',
        },
      }}
    >
      {loading && <CircularProgress />}
      {imageUrl && (
        <img
          style={{
            maxWidth: '100%',
            maxHeight: '60vh',
            visibility: loading ? 'hidden' : 'visible',
          }}
          onError={handleImageLoadingError}
          onLoad={handleImageLoad}
          src={imageUrl}
          alt='randomImage'
        />
      )}
      {!loading && <ButtonGroup variant='outlined' aria-label='outlined button group'>
        <Button color='error' onClick={handleFindAnotherImage}>
          Find another image
        </Button>
        <Button onClick={handleAccept}>Accept</Button>
      </ButtonGroup>}
    </Stack>
  );
};
