import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ImageSelector } from './imageSelector';
import { renderWithProviderAndRouter } from '../../util/redux-test-util';
import { imageActions, ImageState } from '../../store/image-slice';
import { FormState } from '../../store/form-slice';


const mockFetchedData = {
  urls: { regular: 'someNewRegularUrl', thumb: 'someNewThumbnailUrl' },
};

const formState: FormState = {
  name: 'test name',
  surname: 'test surname',
  topic: 'test topic',
};

const imageState: ImageState = {
  urls: {
    regularUrl: 'someRegularUrl',
    thumbnailUrl: 'someThumbnailUrl',
  },
  error: '',
};
const mockedUseNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => mockedUseNavigate,
}));

describe('Image selector component should have correct elements', () => {
  beforeEach(() => {
    jest
      .spyOn(global, 'fetch')
      .mockImplementation(
        jest.fn(() =>
          Promise.resolve({ ok: true, json: () => Promise.resolve(mockFetchedData) })
        ) as jest.Mock
      );

      jest
      .spyOn(imageActions, 'setImageData')
      .mockImplementation(
        jest.fn(() => ({
          type: 'setImageData',
          payload: { regularUrl: 'someNewRegularUrl', thumbnailUrl: 'someNewThumbnailUrl' },
        })) as jest.Mock
      );
  })

  test('renders image and two buttons', async () => {
    renderWithProviderAndRouter(<ImageSelector />, {
      preloadedState: {
        form: formState,
        image: imageState,
      },
    });

    const images = screen.getAllByAltText('randomImage') as HTMLImageElement[];
    expect(images).toHaveLength(1);
    expect(images[0].src).toContain('someRegularUrl');
    fireEvent.load(images[0]);

    const buttons = await screen.findAllByRole('button');
    expect(buttons).toHaveLength(2);

    expect(buttons[0].textContent).toBe('Find another image');
    expect(buttons[1].textContent).toBe('Accept');
  });

  test('clicking the \'Accept\' button causes redirection', async () => {
    const user = userEvent.setup();
    
    renderWithProviderAndRouter(<ImageSelector />, {
      preloadedState: {
        form: formState,
        image: imageState,
      },
    });

    const image = screen.getByAltText('randomImage') as HTMLImageElement;
    fireEvent.load(image);

    const buttons = screen.getAllByRole('button');
    expect(buttons[1].textContent).toBe('Accept');

    await user.click(buttons[1]);

    expect(mockedUseNavigate).toHaveBeenCalledTimes(1);
    expect(mockedUseNavigate).toHaveBeenCalledWith('/image');

    expect(fetch).toHaveBeenCalledTimes(1);
  });

  test('clicking the \'Find another image\' button causes sending an http request', async () => {
    const user = userEvent.setup();

    renderWithProviderAndRouter(<ImageSelector />, {
      preloadedState: {
        form: formState,
        image: imageState,
      },
    });

    const image = screen.getByAltText('randomImage') as HTMLImageElement;
    fireEvent.load(image);

    const buttons = screen.getAllByRole('button');
    expect(buttons[0].textContent).toBe('Find another image');
    await user.click(buttons[0]);
    expect(fetch).toHaveBeenCalledTimes(2);
  });
});
