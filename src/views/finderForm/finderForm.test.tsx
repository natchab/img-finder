import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { FinderForm } from './finderForm';
import { renderWithProviderAndRouter } from '../../util/redux-test-util';

const mockedUseNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom')),
  useNavigate: () => mockedUseNavigate,
}));

const testName = 'test name';
const testSurname = 'test surname';
const testTopic = 'test topic';

describe('the finder form component has correct elements', () => {
  test('has correct form elements', () => {
    renderWithProviderAndRouter(<FinderForm />);

    const inputs = screen.getAllByRole('textbox');
    expect(inputs).toHaveLength(2);
    expect(inputs[0]).toHaveAttribute('id', 'name');
    expect(inputs[1]).toHaveAttribute('id', 'surname');

    const buttons = screen.getAllByRole('button');
    expect(buttons).toHaveLength(4);
    expect(buttons[3].textContent).toBe('Search');
    expect(buttons[3]).toBeDisabled();

    const resetButtons = screen.getAllByLabelText(
      /reset/
    ) as HTMLButtonElement[];
    expect(resetButtons).toHaveLength(2);
    expect(resetButtons[0].type).toBe('button');
    expect(resetButtons[1].type).toBe('button');

    const selectButton = screen.getAllByLabelText(/Topic/);
    expect(selectButton).toHaveLength(1);
  });
});

describe('the form validation works correctly', () => {
  test('blurring inputs with 0 characters shows an error', async () => {
    const user = userEvent.setup();
    renderWithProviderAndRouter(<FinderForm />);

    expect(
      screen.queryByText('The minimum length is 3 characters.')
    ).toBeNull();

    const inputs = screen.getAllByRole('textbox');
    await user.click(inputs[0]);
    await user.click(inputs[1]);

    expect(
      screen.getByText('The minimum length is 3 characters.')
    ).toBeInTheDocument();

    await user.click(inputs[0]);

    expect(
      screen.getAllByText('The minimum length is 3 characters.')
    ).toHaveLength(2);
  });

  test('after providing correct input does not show an error and inputs store correct values', async () => {
    const user = userEvent.setup();
    renderWithProviderAndRouter(<FinderForm />);

    expect(
      screen.queryByText('The minimum length is 3 characters.')
    ).toBeNull();

    const inputs = screen.getAllByRole('textbox');
    await user.type(inputs[0], testName);
    await user.type(inputs[1], testSurname);

    expect(
      screen.queryByText('The minimum length is 3 characters.')
    ).toBeNull();
    expect(inputs[0]).toHaveValue(testName);
    expect(inputs[1]).toHaveValue(testSurname);
  });

  test('choosing values in the select input works correctly', async () => {
    const user = userEvent.setup();
    renderWithProviderAndRouter(<FinderForm />);

    const selectButton = screen.getByLabelText(/Topic/);
    await user.click(selectButton);
    let options = screen.getAllByRole('option');
    expect(options).toHaveLength(5);

    await user.click(options[0]);
    options = screen.queryAllByRole('option');
    expect(options).toHaveLength(0);

    const selectedOption = screen.getByLabelText(/Travel/);
    expect(selectedOption).toBeInTheDocument();
  });

  test('choosing topic \'Other\' results in displaying an additional text input', async () => {
    const user = userEvent.setup();
    renderWithProviderAndRouter(<FinderForm />);

    expect(
      screen.queryByText('The minimum length is 3 characters.')
    ).toBeNull();

    let textBoxes = screen.getAllByRole('textbox');
    const selectButton = screen.getByLabelText(/Topic/);
    expect(textBoxes).toHaveLength(2);
    await user.click(selectButton);
    const options = screen.getAllByRole('option');
    await user.click(options[4]);
    textBoxes = screen.getAllByRole('textbox');
    expect(textBoxes).toHaveLength(3);
    expect(textBoxes[2]).toHaveAttribute('id', 'custom-topic');

    await user.click(textBoxes[2]);
    await user.click(document.body);
    expect(
      screen.getByText('The minimum length is 3 characters.')
    ).toBeInTheDocument();

    await user.type(textBoxes[2], testTopic);
    expect(textBoxes[2]).toHaveValue(testTopic);
    expect(
      screen.queryByText('The minimum length is 3 characters.')
    ).toBeNull();
  });

  test('filling the form correctly enables the \'Search\' button', async () => {
    const user = userEvent.setup();
    renderWithProviderAndRouter(<FinderForm />);

    let textBoxes = screen.getAllByRole('textbox');
    const selectButton = screen.getByLabelText(/Topic/);
    const searchButton = screen.getByText('Search');
    await user.type(textBoxes[0], testName);
    await user.type(textBoxes[1], testSurname);
    await user.click(selectButton);

    let options = screen.getAllByRole('option');
    await user.click(options[1]);
    expect(searchButton).toBeEnabled();

    await user.click(selectButton);
    options = screen.getAllByRole('option');
    await user.click(options[4]);

    textBoxes = screen.getAllByRole('textbox');
    expect(searchButton).toBeDisabled();
    await user.type(textBoxes[2], testTopic);
    expect(searchButton).toBeEnabled();
  });

  test('clicking \'Search\' button redirects to another view', async () => {
    const user = userEvent.setup();
    renderWithProviderAndRouter(<FinderForm />);

    let textBoxes = screen.getAllByRole('textbox');
    const selectButton = screen.getByLabelText(/Topic/);
    const searchButton = screen.getByText('Search');
    await user.type(textBoxes[0], testName);
    await user.type(textBoxes[1], testSurname);
    await user.click(selectButton);
    let options = screen.getAllByRole('option');
    await user.click(options[1]);
    await user.click(searchButton);

    expect(mockedUseNavigate).toHaveBeenCalledTimes(1);
    expect(mockedUseNavigate).toHaveBeenCalledWith('/select-image');
  });
});
