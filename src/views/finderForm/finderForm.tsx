import React, { useState } from 'react';
import {
  Box,
  Button,
  FormControl,
  Stack,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  InputAdornment,
  IconButton,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { useNavigate } from 'react-router-dom';

import { formActions } from '../../store/form-slice';
import useInput from '../../hooks/useInput';
import { useAppDispatch } from '../../hooks/useRedux';
import { hasMinLength, hasMinLengthError } from '../../util/validation';
import { TOPICS } from '../../util/consts';

export const FinderForm = () => {
  const [imgTopic, setImgTopic] = useState('');
  const navigate = useNavigate();

  const dispatch = useAppDispatch();
  const {
    value: nameValue,
    isValid: isNameValid,
    hasError: nameHasError,
    valueChangeHandler: nameChangeHandler,
    blurHandler: nameBlurHandler,
    reset: resetName,
  } = useInput(hasMinLength);
  const {
    value: surnameValue,
    isValid: isSurnameValid,
    hasError: surnameHasError,
    valueChangeHandler: surnameChangeHandler,
    blurHandler: surnameBlurHandler,
    reset: resetSurname,
  } = useInput(hasMinLength);
  const {
    value: customTopicValue,
    isValid: isCustomTopicValid,
    hasError: customTopicHasError,
    valueChangeHandler: customTopicChangeHandler,
    blurHandler: customTopicBlurHandler,
    reset: resetCustomTopic,
  } = useInput(hasMinLength);

  const handleTopicChange = (e: SelectChangeEvent<string>) => {
    setImgTopic(e.target.value);
    if (e.target.value !== TOPICS.OTHER) {
      resetCustomTopic();
    }
  };

  const isFormValid =
    isNameValid &&
    isSurnameValid &&
    (imgTopic === TOPICS.OTHER ? isCustomTopicValid : imgTopic !== '');

  const submitHandler = () => {
    if (!isFormValid) {
      return;
    }
    const formData = {
      name: nameValue,
      surname: surnameValue,
      topic: imgTopic !== TOPICS.OTHER ? imgTopic : customTopicValue,
    };
    dispatch(formActions.setFormData(formData));
    navigate('/select-image');
  };

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        minWidth: '35%',
        alignItems: 'center',
      }}
    >
      <Stack
        sx={{
          minWidth: '35%',
          '& > *': {
            my: '1rem !important',
            width: '100%',
          },
        }}
      >
        <TextField
          error={nameHasError}
          variant='standard'
          label='Name'
          type='text'
          aria-label='name'
          id='name'
          value={nameValue}
          onChange={nameChangeHandler}
          onBlur={nameBlurHandler}
          helperText={nameHasError ? hasMinLengthError : null}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton aria-label='reset name input' onClick={resetName}>
                  <CloseIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <TextField
          error={surnameHasError}
          variant='standard'
          label='Surname'
          type='text'
          id='surname'
          value={surnameValue}
          onChange={surnameChangeHandler}
          onBlur={surnameBlurHandler}
          helperText={
            surnameHasError ? hasMinLengthError : null
          }
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton
                  aria-label='reset surname input'
                  onClick={resetSurname}
                >
                  <CloseIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <FormControl variant='standard'>
          <InputLabel id='demo-simple-select-label'>Topic</InputLabel>
          <Select
            labelId='demo-simple-select-label'
            id='demo-simple-select'
            value={imgTopic}
            label='Topic'
            onChange={handleTopicChange}
          >
            {Object.entries(TOPICS).map(([key, value]) => (
              <MenuItem key={key} value={value}>
                {value}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        {imgTopic === TOPICS.OTHER && (
          <TextField
            error={customTopicHasError}
            variant='standard'
            label='Custom topic'
            type='text'
            id='custom-topic'
            value={customTopicValue}
            onChange={customTopicChangeHandler}
            onBlur={customTopicBlurHandler}
            helperText={
              customTopicHasError ? hasMinLengthError : null
            }
            InputProps={{
              endAdornment: (
                <InputAdornment position='end'>
                  <IconButton
                    aria-label='reset custom topic input'
                    onClick={resetCustomTopic}
                  >
                    <CloseIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        )}
        <Box
          p='1rem'
          alignItems='center'
          justifyContent='center'
          textAlign={'center'}
        >
          <Button
            variant='contained'
            color='primary'
            disabled={!isFormValid}
            onClick={submitHandler}
          >
            Search
          </Button>
        </Box>
      </Stack>
    </Box>
  );
};
