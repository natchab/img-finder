import {
  Box,
  Button,
  Card,
  CardContent,
  CardMedia,
  CircularProgress,
  Typography,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { useAppDispatch, useAppSelector } from '../../hooks/useRedux';
import { imageActions } from '../../store/image-slice';
import { ErrorComponent } from '../errorComponent/errorComponent';

export const ImgCard = () => {
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  
  const imageUrl = useAppSelector((store) => store.image.urls.thumbnailUrl);
  const { name, surname } = useAppSelector((store) => store.form);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!imageUrl) {
      navigate('/');
    }
  }, [imageUrl, navigate]);

  const handleFindNewImage = () => {
    dispatch(imageActions.resetImageData());
    navigate('/');
  };

  const handleImageLoadingError = () => {
    setError('Error: Could not load the image.');
  };

  const handleErrorClose = () => {
    navigate('/');
  };

  const handleImageLoad = (
    e: React.SyntheticEvent<HTMLImageElement, Event>
  ) => {
    setLoading(false);
  };

  if (error) {
    return <ErrorComponent error={error} onErrorClose={handleErrorClose} />;
  }

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > *': {
          m: '1rem',
        },
      }}
    >
      {loading && <CircularProgress />}
      <Card
        sx={{
          display: 'flex',
          visibility: loading ? 'hidden' : '',
          alignItems: 'center',
          minWidth: '40%',
        }}
      >
        <CardContent sx={{ flex: '1 0 auto' }}>
          <Typography component='div' variant='h4'>
            {name}
          </Typography>
          <Typography
            component='div'
            variant='subtitle1'
            color='text.secondary'
          >
            {surname}
          </Typography>
        </CardContent>
        <CardMedia
          component='img'
          sx={{ width: '35%', marginRight: '0.2rem' }}
          image={imageUrl}
          onError={handleImageLoadingError}
          onLoad={handleImageLoad}
          alt='imageThumbnail'
        />
      </Card>
      <Button
        onClick={handleFindNewImage}
        variant='outlined'
        sx={{ visibility: loading ? 'hidden' : '' }}
      >
        Find new image
      </Button>
    </Box>
  );
};
