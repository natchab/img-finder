import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { ImgCard } from './imgCard';
import { renderWithProviderAndRouter } from '../../util/redux-test-util';
import { imageActions, ImageState } from '../../store/image-slice';
import { FormState } from '../../store/form-slice';

const mockData = {
  urls: { regular: 'someNewRegularUrl', thumb: 'someNewThumbnailUrl' },
};

const formState: FormState = {
  name: 'test name',
  surname: 'test surname',
  topic: 'test topic',
};

const imageState: ImageState = {
  urls: {
    regularUrl: 'someRegularUrl',
    thumbnailUrl: 'someThumbnailUrl',
  },
  error: '',
};
const mockedUseNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => mockedUseNavigate,
}));

describe('Image card component should have correct elements', () => {
  beforeEach(() => {
    jest
      .spyOn(global, 'fetch')
      .mockImplementation(
        jest.fn(() =>
          Promise.resolve({ ok: true, json: () => Promise.resolve(mockData) })
        ) as jest.Mock
      );

    jest.spyOn(imageActions, 'setImageData').mockImplementation(
      jest.fn(() => ({
        type: 'setImageData',
        payload: {
          regularUrl: 'someNewRegularUrl',
          thumbnailUrl: 'someNewThumbnailUrl',
        },
      })) as jest.Mock
    );
  });

  test('renders image and a button', async () => {
    renderWithProviderAndRouter(<ImgCard />, {
      preloadedState: {
        form: formState,
        image: imageState,
      },
    });

    const images = screen.getAllByAltText('imageThumbnail') as HTMLImageElement[];
    expect(images).toHaveLength(1);
    expect(images[0].src).toContain('someThumbnailUrl');

    const button = screen.getByText('Find new image') as HTMLButtonElement;
    expect(button.type).toBe('button');
    expect(screen.getByText('test name')).toBeInTheDocument();
    expect(screen.getByText('test surname')).toBeInTheDocument();
  });

  test('clicking the \'Find new image\' button causes redirection', async () => {
    const user = userEvent.setup();

    renderWithProviderAndRouter(<ImgCard />, {
      preloadedState: {
        form: formState,
        image: imageState,
      },
    });

    const button = screen.getByText('Find new image');
    await user.click(button);
    expect(mockedUseNavigate).toHaveBeenCalled();
    expect(mockedUseNavigate).toHaveBeenCalledWith('/');
  });
});
