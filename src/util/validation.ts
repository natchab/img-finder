export const hasMinLength = (value: string) => value.trim().length >= 3;

export const hasMinLengthError = 'The minimum length is 3 characters.'