export const TOPICS = Object.freeze({
  TRAVEL: 'Travel',
  CARS: 'Cars',
  WILDLIFE: 'Wildlife',
  TECHNOLOGY: 'Technology',
  OTHER: 'Other',
});
