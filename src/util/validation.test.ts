import { hasMinLength } from './validation';

test('returns true for strings that are at least 3 characters long', () => {
  expect(hasMinLength('aaa')).toBe(true);
});

test('returns false for strings that are shorter than 3 characters', () => {
  expect(hasMinLength('aa')).toBe(false);
});
