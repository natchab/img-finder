import React from 'react';
import { screen } from '@testing-library/react';

import App from './App';
import { renderWithProviders } from './util/redux-test-util';

test('renders learn react link', () => {
  renderWithProviders(<App />);
  expect(screen.getByText('Image Finder')).toBeInTheDocument();
});
