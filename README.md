# Image finder

A simple React application that allows users to see a random image from Unsplash that matches the chosen topic.

## Prerequisites

In order to run this application, you need to have Node.js installed on your computer. The recommended version is 18. It was tested only on Node.js v18.14.0. 

Another requirement is to generate your Unsplash API key.

## How to run

- Clone this repository
- Create a ```.env``` file with a ```REACT_APP_UNSPLASH_API_KEY``` variable set to your Unsplash API key
- Open the repository directory with Terminal
- Run to install packages:
```console
foo@bar:~$ npm install
```
- Run to start the application:
```console
foo@bar:~$ npm start
```
- For running the tests, please use:
```console
foo@bar:~$ npm run test
```

## Usage

After starting the application the user sees the first view. It is a form with 3 inputs, two of them are text inputs for name and surname, the third one is a select which allows to choose the topic of the image. If the user chooses 'Other', then a new text input appears and a custom topic might be set. All the text inputs have a validation that returns an error in case the text is shorter than 3 characters. The 'Search' button is disabled when any of the inputs is not provided or incorrect.

Once provided the input, the 'Search' button gets enabled and after clicking it, the view changes to another one. It contains a random image matching the given topic and two buttons - one of them fetches another image, the other one accepts the given one and navigates to the next view.

The third view contains a card with the chosen image as a thumbnail, name and surname provided in the form. Below there is a button that navigates back to the form, so that the user can search for another photo.

In case there is an error fetching image data or image itself, the error appears instead of the second/third view, with a button that redirects back to the form.

While the images are loading, there is a spinner indicating that the application is waiting for the image and it will be displayed soon.

## Implementation

The application has a redux store, divided into slices: form and image. The form one stores name, surname and the chosen topic, the image one - url for the regular and thumbnail versions of the chosen image as well as error, in case it occurs when fetching image data.

All the text inputs in the form have the same validation condition - at least 3 characters should be provided. Validation is triggered on blur first time the user inputs a value, on keystroke when changing it.

When fetching image data, a custom action creator is dispatched instead of a regular function to make it possible to make a request. After data is received, it dispatches an action to store image urls in the store.

The application fetches images from the Unsplash API (/photos/random) and needs to have API key provided. It's not part of the repository, as it should never be shared.

React Router v6 has been used to navigate between the views. Hovewer, user is not allowed to visit the views displaying photos without providing form data before. This will result in navigating to the first view. The same applies to visiting a url that does not exist.

The aplication is tested with Jest and React Testing Library. There are tests for components, util functions as well as for the redux store.

As for styling, the application uses Material UI.

## Future steps
The application functionality can be extended. One idea would be to display a list of cards with all the images that a user has chosen.
From the technical point of view, a GitLab pipeline could be added. It could for example be set to run the tests each time somebody pushes a new commit to the repository. Also, more tests could be implemented to test all the features and states of components.